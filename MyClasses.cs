﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gpx_track
{
    class TrkPoint
    {
        public double lat;
        public double lon;
        public double ele;
        public DateTime dt;
        public TrkPoint(string lat, string lon, string ele, string dt)
        {
            this.lat = double.Parse(lat);
            this.lon = double.Parse(lon);
            if (ele != null)
                this.ele = double.Parse(ele);
            if (dt != null)
                this.dt = DateTime.Parse(dt);
        }
    }

    class UTM
    {
        public double x;
        public double y;
        public double lat;
        public double lon;
        // the following parameters taken from http://www.whoi.edu/marine/ndsf/utility/NDSFref.txt
        private const double RADIUS = 6378137.0;
        private const double FLATTENING = 0.00335281068; /* GRS80 or WGS84 */
        private const double K_NOT = 0.9996;     /* UTM scale factor */
        //private const double DEGREES_TO_RADIANS = 0.01745329252;
        private const double FALSE_EASTING = 500000.0;
        private const double FALSE_NORTHING = 10000000.0;
        private int zone0;      // There are 60 longitudinal projection zones numbered 1 to 60 starting at 180°W. 
        // Each of these zones is 6 degrees wide, apart from a few exceptions around Norway and Svalbard. 
        private char zone1;     // There are 20 latitudinal zones spanning the latitudes 80°S to 84°N and denoted by the letters C to X, 
        // ommitting the letter O. Each of these is 8 degrees south-north, apart from zone X which is 12 degrees south-north.
        public string Zone { get { return zone0.ToString() + zone1; } }
        public UTM(double lat, double lon, bool zone = false)
        {
            if (zone0 == 0 || zone)
            {
                zone0 = (int)(180 + lon) / 6 + 1;
                if (zone0 > 60) zone0 = 60;
                if (lat >= -80 && lat < 84)
                {
                    zone1 = (char)(0x43 + (lat + 90) / 8);
                    if (zone1 >= 'O') zone1++;
                }
                else if (lat > -80 && lat <= 84) zone1 = 'X';
                else zone1 = 'Z';
            }
            this.lat = lat;
            this.lon = lon;
            lat = lat / 180 * Math.PI;
            lon = lon / 180 * Math.PI;
            var lambda_not = ((-180.0 + zone0 * 6.0) - 3.0) / 180 * Math.PI;
            var e_squared = 2.0 * FLATTENING - FLATTENING * FLATTENING;
            var e_fourth = e_squared * e_squared;
            var e_sixth = e_fourth * e_squared;
            var e_prime_sq = e_squared / (1.0 - e_squared);
            var sin_phi = Math.Sin(lat);
            var tan_phi = Math.Tan(lat);
            var cos_phi = Math.Cos(lat);
            var N = RADIUS / Math.Sqrt(1.0 - e_squared * sin_phi * sin_phi);
            var T = tan_phi * tan_phi;
            var C = e_prime_sq * cos_phi * cos_phi;
            var M = RADIUS * ((1.0 - e_squared * 0.25 - 0.046875 * e_fourth - 0.01953125 * e_sixth) * lat -
                            (0.375 * e_squared + 0.09375 * e_fourth + 0.043945313 * e_sixth) * Math.Sin(2.0 * lat) +
                            (0.05859375 * e_fourth + 0.043945313 * e_sixth) * Math.Sin(4.0 * lat) -
                            (0.011393229 * e_sixth) * Math.Sin(6.0 * lat));
            var A = (lon - lambda_not) * cos_phi;
            var A_sq = A * A;
            var A_fourth = A_sq * A_sq;

            /* now go ahead and compute X and Y */
            x = K_NOT * N * (A + (1.0 - T + C) * A_sq * A / 6.0 +
                (5.0 - 18.0 * T + T * T + 72.0 * C - 58.0 * e_prime_sq) * A_fourth * A / 120.0);

            /* note:  specific to UTM, vice general trasverse mercator.  
                since the origin is at the equator, M0, the M at phi_0, 
                always equals zero, and I won't compute it   */
            y = K_NOT * (M + N * tan_phi * (A_sq / 2.0 + (5.0 - T + 9.0 * C + 4.0 * C * C) * A_fourth / 24.0 +
                (61.0 - 58.0 * T + T * T + 600.0 * C - 330.0 * e_prime_sq) * A_fourth * A_sq / 720.0));

            /* now correct for false easting and northing */
            if (lat < 0) y += 10000000.0;
            x += 500000;
        }

        public UTM(double x, double y, string zone)
        {
            this.x = x;
            this.y = y;
            zone1 = zone[zone.Length - 1];
            int.TryParse(zone.Substring(0, zone.Length - 1), out zone0);

            /* first, subtract the false easting */
            x = x - FALSE_EASTING;

            /* compute the necessary geodetic parameters and constants */
            var e_squared = 2.0 * FLATTENING - FLATTENING * FLATTENING;
            var e_fourth = e_squared * e_squared;
            var e_sixth = e_fourth * e_squared;
            var oneminuse = Math.Sqrt(1.0 - e_squared);

            /* compute the footpoint latitude */
            var M = y / K_NOT;
            var mu = M / (RADIUS * (1.0 - 0.25 * e_squared - 0.046875 * e_fourth - 0.01953125 * e_sixth));
            var e1 = (1.0 - oneminuse) / (1.0 + oneminuse);
            var e1sq = e1 * e1;
            var footpoint = mu + (1.5 * e1 - 0.84375 * e1sq * e1) * Math.Sin(2.0 * mu) +
                    (1.3125 * e1sq - 1.71875 * e1sq * e1sq) * Math.Sin(4.0 * mu) +
                    (1.57291666667 * e1sq * e1) * Math.Sin(6.0 * mu) +
                    (2.142578125 * e1sq * e1sq) * Math.Sin(8.0 * mu);


            /* compute the other necessary terms */
            var e_prime_sq = e_squared / (1.0 - e_squared);
            var sin_phi = Math.Sin(footpoint);
            var tan_phi = Math.Tan(footpoint);
            var cos_phi = Math.Cos(footpoint);
            var N = RADIUS / Math.Sqrt(1.0 - e_squared * sin_phi * sin_phi);
            var T = tan_phi * tan_phi;
            var Tsquared = T * T;
            var C = e_prime_sq * cos_phi * cos_phi;
            var Csquared = C * C;
            var denom = Math.Sqrt(1.0 - e_squared * sin_phi * sin_phi);
            var R = RADIUS * oneminuse * oneminuse / (denom * denom * denom);
            var D = x / (N * K_NOT);
            var Dsquared = D * D;
            var Dfourth = Dsquared * Dsquared;

            var lambda_not = ((-180.0 + zone0 * 6.0) - 3.0) / 180 * Math.PI;

            /* now, use the footpoint to compute the real latitude and longitude */
            lat = footpoint - (N * tan_phi / R) * (0.5 * Dsquared - (5.0 + 3.0 * T + 10.0 * C -
                                 4.0 * Csquared - 9.0 * e_prime_sq) * Dfourth / 24.0 +
                                 (61.0 + 90.0 * T + 298.0 * C + 45.0 * Tsquared -
                                  252.0 * e_prime_sq -
                                  3.0 * Csquared) * Dfourth * Dsquared / 720.0);
            lon = lambda_not + (D - (1.0 + 2.0 * T + C) * Dsquared * D / 6.0 +
                               (5.0 - 2.0 * C + 28.0 * T - 3.0 * Csquared + 8.0 * e_prime_sq +
                                24.0 * Tsquared) * Dfourth * D / 120.0) / cos_phi;

            lat = lat * 180 / Math.PI;
            lon = lon * 180 / Math.PI;
        }
    }

    class Gmaps_Scales 
    {
        // taken from http://webhelp.esri.com/arcgisserver/9.3/java/gmaps_ve_scales.txt on Oct17'17
        public static double[] gmaps_ve_scales = {   591657550.5, 36978596.91, 73957193.82, 147914387.6, 295828775.3,
                                     18489298.45, 9244649.227, 4622324.614, 2311162.307, 1155581.153,
                                     577790.5767, 288895.2884, 144447.6442, 72223.82209, 36111.91104,
                                     18055.95552, 9027.977761, 4513.988880, 2256.994440, 1128.497220 };

        // m/ pixel taken from http://wiki.openstreetmap.org/wiki/Zoom_levels on Oct18'17
        public static double[] mppx = { /*156412, 78206, 39103, 19551, 9776,
                                           4888, 2444, 1222, 610.984, 305.492,
                                           152.746, 76.373, 38.187, 19.093, 9.547,
                                           4.773, 2.387, 1.193, 0.596, 0.298 };*/
                                          21282, 16355, 10064, 5540, 2909, 
                                          1485, 752, 378, 190, 95,
                                          48, 24, 12, 6, 3,
                                          1.48, 0.74, 0.37, 0.19};
    }
}
