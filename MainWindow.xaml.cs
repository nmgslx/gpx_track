﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.Xml.XPath;
using System.Text.RegularExpressions;

namespace gpx_track
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            var list = new List<String>();
            list.Add("Google Maps");
            list.Add("Baidu Maps");
            comboBoxScale.ItemsSource = list;
            comboBoxScale.SelectedIndex = 0;
            load_gpx();
        }
        private void load_gpx()
        {
            list = new List<TrkPoint>();
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(textBoxFilename.Text);
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(doc.NameTable);
                var nav = doc.CreateNavigator();
                nav.MoveToFollowing(XPathNodeType.Element);
                string default_prefix = "";
                foreach (var z in nav.GetNamespacesInScope(XmlNamespaceScope.All))
                {
                    if (z.Key == String.Empty)
                    {
                        default_prefix = "x:";
                        nsmgr.AddNamespace("x", "http://www.topografix.com/GPX/1/0"); //default namespace
                    }
                }
                XmlNodeList nodes = doc.SelectNodes(String.Format("//{0}trkseg/{0}trkpt", default_prefix), nsmgr);
                foreach (XmlNode n in nodes)
                {
                    var ele = n.SelectSingleNode(default_prefix + "ele", nsmgr);
                    var tm = n.SelectSingleNode(default_prefix + "time", nsmgr);
                    var pt = new TrkPoint(n.Attributes["lat"].Value, n.Attributes["lon"].Value, ele == null ? null : ele.InnerText, tm == null ? null : tm.InnerText);
                    list.Add(pt);
                }

                points = new Point[list.Count];
                Point L_T = new Point(double.MaxValue, double.MaxValue);
                Point R_B = new Point(-double.MaxValue, -double.MaxValue);
                for (int i = 0; i < list.Count; i++)
                {
                    var xy = new UTM(list[i].lat, list[i].lon);
                    points[i] = new Point(xy.x, xy.y);
                    if (L_T.X > points[i].X) L_T.X = points[i].X;
                    if (L_T.Y > points[i].Y) L_T.Y = points[i].Y;
                    if (R_B.X < points[i].X) R_B.X = points[i].X;
                    if (R_B.Y < points[i].Y) R_B.Y = points[i].Y;
                }
                CC = new Point((L_T.X + R_B.X) / 2, (L_T.Y + R_B.Y) / 2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Open/Read XML file error");
            }
        }
        private bool isMax = false;
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            isMax = !isMax;
            if (isMax)
            {
                WindowState = WindowState.Maximized;
                button1.Content = "◽";
            }
            else 
            { 
                WindowState = WindowState.Normal;
                button1.Content = "☐";
            }
            repaint();
        }
        private Brush color1 = new SolidColorBrush(Color.FromArgb(0xFF, 0x00, 0xBB, 0xFF));
        private List<TrkPoint> list;
        private Point[] points;
        private Point CC;
        private Point cc;
        private void repaint()
        {
            canvas1.Children.Clear();
            Rectangle rect = new Rectangle();
            rect.Height = main.Height;
            rect.Width = main.Width;
            rect.StrokeThickness = 2;
            rect.Stroke = color1;
            Canvas.SetLeft(rect, 0);
            Canvas.SetTop(rect, 0);
            canvas1.Children.Add(rect);
            
            for (int i = 1; i < points.Length; i++)
            {
                var line = new Line();
                line.Stroke = Brushes.Green;
                line.StrokeThickness = 3;
                line.X1 = (points[i - 1].X - CC.X) / MPX + cc.X;
                line.Y1 = (CC.Y - points[i - 1].Y) / MPX + cc.Y;
                line.X2 = (points[i].X - CC.X) / MPX + cc.X;
                line.Y2 = (CC.Y - points[i].Y) / MPX + cc.Y;
                canvas1.Children.Add(line);
            }
            tboxScale.Text = scale.ToString();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            repaint();
        }

        private bool labelMoveMouseDown = false;
        private bool labelSizeMouseDown = false;
        private bool canvasMouseDown = false;
        private Point lastMovePosition;
        private void labelMove_MouseDown(object sender, MouseButtonEventArgs e)
        {
            labelMoveMouseDown = true;
            lastMovePosition = e.GetPosition(this);
            main.CaptureMouse();
        }

        private void main_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (labelMoveMouseDown)
            {
                labelMoveMouseDown = false;
                main.ReleaseMouseCapture();
                var p = e.GetPosition(this);
                main.Top = main.Top + p.Y - lastMovePosition.Y;
                main.Left = main.Left + p.X - lastMovePosition.X;
            }
            else if (labelSizeMouseDown)
            {
                labelSizeMouseDown = false;
                main.ReleaseMouseCapture();
                var p = e.GetPosition(this);
                double w = main.Width + p.X - lastMovePosition.X;
                double h = main.Height + p.Y - lastMovePosition.Y;
                if (w < 200) w = 200;
                if (h < 100) h = 100;
                main.Width = w;
                main.Height = h;
                repaint();
            }
            else if (canvasMouseDown)
            {
                canvasMouseDown = false;
                main.ReleaseMouseCapture();
                var p = e.GetPosition(this);
                cc.X += p.X - lastMovePosition.X;
                cc.Y += p.Y - lastMovePosition.Y;
                repaint();
            }
            Cursor = Cursors.Arrow;
        }
        private int scale = 14;
        private double[] mpx = { 1.75, 1.6 };
        private int map = 0;
        double MPX { get { return mpx[map]*Math.Pow(2,14-scale);} }
        private void labelSize_MouseDown(object sender, MouseButtonEventArgs e)
        {
            labelSizeMouseDown = true;
            lastMovePosition = e.GetPosition(this);
            main.CaptureMouse();
        }

        private void labelZoomIn_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (scale < Gmaps_Scales.mppx.Length - 1)
            {
                scale++;
                repaint();
            }
        }

        private void labelZoomOut_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (scale > 0)
            {
                scale--;
                repaint();
            }
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            if (grid1.Visibility == Visibility.Hidden) {
                button3.Content = "▲";
                grid1.Visibility = Visibility.Visible; 
            }
            else
            {
                button3.Content = "▼";
                grid1.Visibility = Visibility.Hidden;
            }
        }
        private bool not_fire;
        private void textBoxLatLon_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (not_fire) return;
            textBoxZone.Text = "?";
            if (String.IsNullOrWhiteSpace(textBoxLat.Text) || String.IsNullOrWhiteSpace(textBoxLon.Text)) return;
            double lat,lon;
            if (!double.TryParse(textBoxLat.Text, out lat) || !double.TryParse(textBoxLon.Text, out lon)) return;
            var utm = new UTM(lat, lon, true);
            not_fire = true;
            textBoxZone.Text = utm.Zone;
            textBoxX.Text = utm.x.ToString("0,0.00");
            textBoxY.Text = utm.y.ToString("0,0.00");
            not_fire = false;
        }

        private void textBoxZoneXY_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (not_fire) return;
            if (!Regex.IsMatch(textBoxZone.Text, @"^\d+[A-Za-z]$") || String.IsNullOrWhiteSpace(textBoxX.Text) || String.IsNullOrWhiteSpace(textBoxY.Text)) return;
            double x, y;
            if (!double.TryParse(textBoxX.Text, out x) || !double.TryParse(textBoxY.Text, out y)) return;
            var utm = new UTM(x, y, textBoxZone.Text);
            not_fire = true;
            textBoxLat.Text = utm.lat.ToString("0.000000");
            textBoxLon.Text = utm.lon.ToString("0.000000");
            not_fire = false;
        }

        private void tboxScale_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            int.TryParse(tboxScale.Text, out scale);
            repaint();
        }

        private void canvas1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            canvasMouseDown = true;
            lastMovePosition = e.GetPosition(this);
            main.CaptureMouse();
        }

        private void main_SizeChanged(object sender, SizeChangedEventArgs e)
        {
             cc = new Point(Width / 2, Height / 2);
        }

        private void textBoxFilename_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;
            load_gpx();
            repaint();
        }

        private void labelSize_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.SizeAll;
        }

        private void labelSize_MouseLeave(object sender, MouseEventArgs e)
        {
            if (!labelSizeMouseDown)
                Cursor = Cursors.Arrow;
        }

        private void canvas1_MouseEnter(object sender, MouseEventArgs e)
        {
            Cursor = Cursors.ScrollAll;
        }

        private void comboBoxScale_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            map = comboBoxScale.SelectedIndex;
            if (points!=null) repaint();
        }

        private void comboBoxScale_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!labelMoveMouseDown)
                Cursor = Cursors.Arrow;
        }
        private double zoom = 1;
        private void canvas_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            if (e.Delta < 0 && zoom > 0.25) zoom = zoom - 0.01;
            if (e.Delta > 0 && zoom < 2) zoom = zoom + 0.01;
            grid0.LayoutTransform = new ScaleTransform(zoom, zoom);
            grid0.UpdateLayout();
        }
    }
}
